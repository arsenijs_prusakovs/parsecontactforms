<?php
require('simplehtmldom/simple_html_dom.php');
libxml_use_internal_errors(true);

$row = array();
$row = inputCsv();
$array = array();

foreach ($row as $rowData) {
    echo "Sending to:" . $rowData[0] . "\n";
    fillForm($rowData);
}


function inputCsv()
{
    $row = array();
    if (($handle = fopen("output.csv", "r")) !== false) {
        while (($data = fgetcsv($handle, 1000, ",")) !== false) {
            $row[] = array($data[0], $data[1], $data[2], $data[3]);
        }
        fclose($handle);
    }
    return $row;
}

function fillForm($rowData)
{

    $url = $rowData[3];
    $fields = array(
        'telephone' => '+37126653474',
        'name' => 'Vyacheslav Kreidikov',
        'email' => 'vjaceslavs@scandiweb.com',
        'comment' => $rowData[1],
        'company' => 'Scandiweb',
        'hideit' => ''
    );

    //open connection
    $ch = curl_init();

    //set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
   // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

    //execute post
    $result = curl_exec($ch);
     var_dump(curl_getinfo($ch, CURLINFO_HTTP_CODE));
    //close connection
    curl_close($ch);
}