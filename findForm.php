<?php
require('simplehtmldom/simple_html_dom.php');
libxml_use_internal_errors(true);

$row = array();
$urls = array();

$fileName = "input.csv"; // input file

$row = inputCsv($fileName);
$array = array();

// Use i < count($row) to loop all array
for ($i = 0; $i < count($row); $i++) {
    if (checkContactUrl('http://'.$row[$i])) {
        echo 'http://'.$row[$i]."\n";
    }
    else if (checkContactUrl('https://'.$row[$i])) {
        echo 'https://'.$row[$i]."\n";
    }
    else if (checkContactUrl('http://www.'.$row[$i])) {
        echo 'http://www.'.$row[$i]."\n";
    }
    else if (checkContactUrl('https://www.'.$row[$i])) {
        echo 'https://www.'.$row[$i]."\n";
    }
    else {
        echo $row[$i]." - Not found"."\n";
    }
}

function remoteFileExists($url)
{
    $url .= "/sitemap.xml";
    $curl = curl_init($url);

    //don't fetch the actual page, you only want to check the connection is ok
    curl_setopt($curl, CURLOPT_NOBODY, true);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_TIMEOUT, 3);
    //do request
    try {
        $result = curl_exec($curl);

        $ret = false;

        //if request did not fail
        if ($result !== false) {
            //if request was ok, check response code
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if ($statusCode == 200) {
                $ret = true;
            }
        }

        curl_close($curl);

        return $ret;
    } catch (Exception $e) {
        echo 'Can not load ' . $url . "\n";
    }
}

function urlExists($url)
{
    $parts = parse_url($url);
    if (!$parts) {
        return false;
    } /* the URL was seriously wrong */

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);

    /* set the user agent - might help, doesn't hurt */
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    /* try to follow redirects */
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

    /* timeout after the specified number of seconds. assuming that this script runs
      on a server, 20 seconds should be plenty of time to verify a valid URL.  */
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 13);
    curl_setopt($ch, CURLOPT_TIMEOUT, 18);

    /* don't download the page, just the header (much faster in this case) */
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_HEADER, true);

    /* handle HTTPS links */
    if ($parts['scheme'] == 'https') {
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    }

    $response = curl_exec($ch);

    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header = substr($response, 0, $header_size);

    curl_close($ch);

    /*  get the status code from HTTP headers */
    if (preg_match('/HTTP\/1\.\d+\s+(\d+)/', $response, $matches)) {
        $code = intval($matches[1]);
    }
    else {
        return false;
    };

    /* see if code indicates success */
    return (($code >= 200) && ($code < 400));
}

function remoteFormExists($url)
{
    try {
        $forms = array();
        $html = get_data($url);
        if (!$html) {
            return '';
        }
        foreach ($html->find('form') as $form) {
            if (strpos($form->action, 'contact')) {
                $forms[] = $form->action;
            }
        }
        return $forms;
    } catch (Exception $e) {
        echo 'Can not load page' . $url . "\n";
    }
}

function get_data($url)
{
    $ch = curl_init();
    $timeout = 7;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $data = curl_exec($ch);
    curl_close($ch);
    $data = str_get_html($data);
    return $data;
}

function inputCsv($fileName)
{
    $row = array();
    if (($handle = fopen($fileName, "r")) !== false) {
        while (($data = fgetcsv($handle, 1000, ",")) !== false) {
            $row[] = $data[0];
        }
        fclose($handle);
    }
    return $row;
}

function checkContactUrl($domain) {

    if ( urlExists($domain."/contacts") && (getForms($domain."/contacts")))  {
        return true;
    }
    if ( urlExists($domain."/contact-us") && (getForms($domain."/contact-us"))) {
        return true;
    }
    if ( urlExists($domain."/contact") && (getForms($domain."/contact"))) {
        return true;
    }
    return false;
}

function getForms($url) {
    $result = false;
    $formUrls = remoteFormExists($url);
    foreach ($formUrls as $formUrl) {
        $out = fopen('output.csv', 'a');
        fputcsv($out, array($url, $formUrl));
        fclose($out);
        echo $formUrl."\n";
        $result = true;
    }

    return $result;
}