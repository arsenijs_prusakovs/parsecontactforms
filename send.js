

var fs = require('fs'),
    system = require('system'),
    loadInProgress = false;

if (system.args.length < 2) {
    console.log("UsagereareadFile.js FILE");
    phantom.exit(1);
}

var content = '',
    f = null,
    lines = null,
    eol = system.os.name == 'windows' ? "\r\n" : "\n";

try {
    f = fs.open(system.args[1], "r");
    content = f.read();
} catch (e) {
    console.log(e);
}

if (f) {
    f.close();
}

var answer = '-';
var arrData = CSVToArray(content);
var newText = '\n\nHello!\n\n'+

'We have noticed that your E-commerce platform is based on Magento, we hope it\'s completely fulfilling your needs.\n\n'+

'It happens that from time to time your company needs some minor development help (to put Magento updates, fix errors/bugs, etc.) as well as continuous improvement.\n\n'+

'We work with such companies like Jaguar, Land Rover, New York Times, Reuters, Lafayette 148 NY, Country Casual, and many many more. Some of our recent projects:\n'+
'www.jysk.ca - biggest modern style furniture and accessories chain in Canada with 50+ stores\n'+
'www.lafayette148ny.com - leading fashion retailer in US, outfits module\n'+
'www.gsioutdoors.com - biggest outdoor equipment brand in the USA\n'+
'www.candy.com - domain alone costs six figure number. Most popular place where to get snacks online\n\n'+

'All customers and testimonials can be found here - http://scandiweb.com/team#client-testimonials\n\n'+

'That\'s where we have greatest expertise and experience!\n'+

'Join our PREMIUM support now and get:\n'+
'- time to action on showstopper issues on your store 1-2 hours\n'+
'- fast implementation of change requests - 1-3 days\n'+
'- monthly consultancy on eCommerce best practices and enhancements for your store\n'+
'- proactive server and application monitoring with New Relic\n'+
'- stability, we are based in Europe and US and serve clients such as Reuters, Lego, JYSK.\n'+
'- excellent code quality, 107 Certified Magento Developers on board\n\n'+

'How to apply?\n'+
'Send a request and your Contact Details to info@scandiweb.com or setup a meeting @ scandiweb.com/contact\n\n'+

'Hope to hear from you soon,\n'+
'Scandiweb developers team';
if (content) {
    var len = arrData.length;
    fs.write('new.csv', 'Action url,POST status, Message status, Input count, Select count, Message text' , 'a');
    var i = 0;

    interval = setInterval(function() {
      if (!loadInProgress && i < len) {
        loadInProgress = true;
        sentPost(arrData[i][1], newText);
        i++;
      }
      if (i == len) {
        console.log("test complete!");
        phantom.exit();
      }
    }, 100);
}


function sentPost(url, emailText){

  var page = require('webpage').create(),
      server = url,
      data = 'name=Vyacheslav Kreidikov&'+
             'telephone=0037126653474&'+
             'company=Scandiweb&'+
             'email=vjaceslavs@scandiweb.com&'+
             'message='+emailText+'&'+
             'subject=24/7 Magento support by a team of 27 Certified Magento Developers Plus&'+
             'comment='+emailText+'&hideit=';

  page.settings.resourceTimeout = 7000; // 7 seconds
  page.settings.loadImages = false;

  page.onResourceTimeout = function() {
    fs.write('new.csv', '\n'+server+',Time limit exceed for getting a response', 'a');
    loadInProgress = false;
    console.log('Time limit exceed');
    page.close();
  };

page.onError = function(msg, trace) {

  var msgStack = ['ERROR: ' + msg];

  loadInProgress = false;
  console.log(msgStack);
  page.close();
};

  page.open(server, 'post', data, function (status) {
      console.log("\nCurrent site:"+server);
      url = server;
      if (status !== 'success') {
          answer = 'Unable to post';
      }
      else {
          answer = 'Message posted';
      }
	  console.log(answer);
  });

  page.onLoadFinished = function(status) {
    if (status === 'success') {
      page.includeJs('http://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js', function() {
        var messageResult = page.evaluate(function() {
              if ($('.success-msg').length) {
                  return "Success";
              }
              else if ($('.error-msg').length) {
                  return "Fail";
              }

          return "Unknown";
        });

        var inputCount = page.evaluate(function(s) {
          return $("form").filter("[action='"+s+"']").find('input, textarea').length;
        },server);

        var selectCount = page.evaluate(function(s) {
          return $("form").filter("[action='"+s+"']").find('select').length;
        },server);

        var messageText = page.evaluate(function() {
          if ($('.success-msg').length) {
              return $('.success-msg').html();
          }
          else if ($('.error-msg').length) {
              return $('.error-msg').html();
          }
          return "Can't get a message";
        });

        messageText = messageText.replace(/(\r\n|\n|\r|,|;)/gm,'').slice(0,160);

        fs.write('new.csv', '\n'+server+','+answer+','+messageResult+','+inputCount+','+selectCount+','+messageText, 'a');
        loadInProgress = false;
    		console.log('OnLoad connection success.');
    		page.close();
      });
    } else {
      fs.write('new.csv', '\n'+server+',Conection failed', 'a');
      loadInProgress = false;
  	  console.log('OnLoad connection failed.');
  	  page.close();
    }
  };
}

function CSVToArray( strData, strDelimiter ){
      // Check to see if the delimiter is defined. If not,
      // then default to comma.
      strDelimiter = (strDelimiter || ",");

      // Create a regular expression to parse the CSV values.
      var objPattern = new RegExp(
          (
              // Delimiters.
              "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

              // Quoted fields.
              "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

              // Standard fields.
              "([^\"\\" + strDelimiter + "\\r\\n]*))"
          ),
          "gi"
          );
      // Create an array to hold our data. Give the array
      // a default empty first row.
      var arrData = [[]];

      // Create an array to hold our individual pattern
      // matching groups.
      var arrMatches = null;


      // Keep looping over the regular expression matches
      // until we can no longer find a match.
      while (arrMatches = objPattern.exec( strData )){

          // Get the delimiter that was found.
          var strMatchedDelimiter = arrMatches[ 1 ];

          // Check to see if the given delimiter has a length
          // (is not the start of string) and if it matches
          // field delimiter. If id does not, then we know
          // that this delimiter is a row delimiter.
          if (
              strMatchedDelimiter.length &&
              strMatchedDelimiter !== strDelimiter
              ){

              // Since we have reached a new row of data,
              // add an empty row to our data array.
              arrData.push( [] );

          }

          var strMatchedValue;

          // Now that we have our delimiter out of the way,
          // let's check to see which kind of value we
          // captured (quoted or unquoted).
          if (arrMatches[ 2 ]){

              // We found a quoted value. When we capture
              // this value, unescape any double quotes.
              strMatchedValue = arrMatches[ 2 ].replace(
                  new RegExp( "\"\"", "g" ),
                  "\""
                  );

          } else {

              // We found a non-quoted value.
              strMatchedValue = arrMatches[ 3 ];

          }


          // Now that we have our value string, let's add
          // it to the data array.
          arrData[ arrData.length - 1 ].push( strMatchedValue );
      }

      // Return the parsed data.
      return( arrData );
  }
