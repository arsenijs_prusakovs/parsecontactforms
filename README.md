# README #

Use findForm.php to collect contact forms on magento sites. For input it required a csv (see email.csv as example) file where:
1 column - domain;
2 column - email text;
In output it create csv (by default output.csv) file where:
1 column - domain;
2 column - email text;
3 column - form url;
4 column - form action url;

Use sentForm.php to fill and and send form. As input file it uses output.csv from findForm.php script. It takes email text from 2 column and action url from 4 and send form. 